$('.our-services-depart').on('click',(event)=> {
    $('.our-services-depart').each(function(elemIndex){
        if(this === event.target){
            $(event.target).addClass('active');
            $('.ul-content li').eq(elemIndex).css('display','block');
            $('.content-photo').eq(elemIndex).css('display','block');
        }
        else{
            $(this).removeClass('active');
            $('.ul-content li').eq(elemIndex).css('display','none');
            $('.content-photo').eq(elemIndex).css('display','none');
        }

    });
});
$('.ul-content li:eq(0)').css('display','block');
$('.content-photo:eq(0)').css('display','block');


const navBarMenu = document.querySelector('.amazing-work-navbar-menu-flexbox');
const navBarMenuItem = document.querySelectorAll('.amazing-work-categories-items');
const allCategories = document.querySelectorAll('.all');

navBarMenu.addEventListener('click',(event)=>{
    const tabTitle = event.target.getAttribute('data-about');
    document.querySelector('.amazing-work-categories-items.active').classList.remove('active');

    event.target.classList.add('active');
    allCategories.forEach(node=> {

        if(node.classList.contains(tabTitle) && !node.classList.contains('amazing-work-display-none')){
            node.style.display = 'block';
        }
        else{
            node.style.display = 'none'
        }

    })

});



const buttonLoadMore = document.querySelector('.button-load-more');
const twelveImages = document.querySelectorAll('.amazing-work-display-none');
buttonLoadMore.addEventListener('click',()=>{
    twelveImages.forEach(node => {
        node.classList.remove('amazing-work-display-none');
        node.style.display = 'block';
    }
    );
        buttonLoadMore.style.display = 'none';
});

$(document).ready(function(){
    $('.sl').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.sl-nav'
    });
    $('.sl-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.sl',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        nextArrow: '<i id="fa-angle-right" class="fa fa-angle-right" aria-hidden="true"></i>',
        prevArrow: '<i id="fa-angle-left" class="fa fa-angle-left" aria-hidden="true"></i>'
    });
});

// const slideBar = document.querySelector('sl--nav');
// slideBar.addEventListener('click', (evt) => {
//     document.querySelector('.sl--img-2.active').classList.remove('active');
//     evt.target.classlist.add('active')
// });
$('.sl--img-2').on('click',(event)=> {
    $('.sl--img-2').each(function(elemIndex){
        if(this === event.target){
            $(event.target).addClass('active');
        }
        else{
            $(this).removeClass('active');
        }

    });
});





